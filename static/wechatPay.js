export function wechatPay(orderInfo) {
      // 存储支付信息到安卓webview和IOS     
      const isAndroid = navigator.userAgent.indexOf('Android') > -1 || navigator.userAgent.indexOf('Adr') > -1; //android终端
      const isIOS = !!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
      if (isAndroid) {
        JS.wechatPay(orderInfo);
      } else if (isIOS) {
        this.$bridge.callHandler("wechatPay", orderInfo);
      }else{
        console.log('不是安卓系统也不是苹果系统');
      }
    }
